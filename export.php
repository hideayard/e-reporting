<?php

session_start();

if (isset($_SESSION['HTTP_USER_AGENT']))
{
    if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT']))
    {
        /* Prompt for password */
        exit;
    }
}
else
{
    $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
}

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
include_once ("config/functions.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);

$file = basename($_SERVER['PHP_SELF']);
$filename = (explode(".",$file))[0];
$id_user=isset($_SESSION['i']) ? $_SESSION['i'] : ""; 
$tipe=isset($_SESSION['t']) ? $_SESSION['t'] : ""; 

// if(!check_role($filename,''))
// {
//   echo json_encode( array("status" => false,"info" => "You are not authorized.!!!","messages" => "You are not authorized.!!!" ) );
// }
// else
{
    $created_by = " oshe_created_by = ".$id_user;

    if($tipe == "ADMIN"||$tipe == "HQ")
    {
        $created_by = " oshe_is_deleted = 0 ";
    }
    $txt_header= "oshe_created_at,oshe_tarikh,oshe_email,oshe_pejabat,oshe_majikan,oshe_kod_majikan,oshe_bil_peserta,oshe_ca,oshe_ia,oshe_checkbox1,oshe_checkbox2,oshe_checkbox3,oshe_checkbox4,oshe_checkbox5,oshe_checkbox6,oshe_checkbox7,oshe_checkbox8,oshe_checkbox8_text,oshe_cadangan,oshe_photo1";
    $txt_query= "DATE_FORMAT(oshe_created_at, '%d/%m/%Y') as oshe_created_at ,DATE_FORMAT(oshe_tarikh, '%d/%m/%Y') as oshe_tarikh,oshe_email,oshe_pejabat,oshe_majikan,oshe_kod_majikan,oshe_bil_peserta,oshe_ca,oshe_ia,oshe_checkbox1,oshe_checkbox2,oshe_checkbox3,oshe_checkbox4,oshe_checkbox5,oshe_checkbox6,oshe_checkbox7,oshe_checkbox8,oshe_checkbox8_text,oshe_cadangan,oshe_photo1";

    $label = explode(",","TARIKH LAPOR,TARIKH PROGRAM,EMAIL,PEJABAT,MAJIKAN,KOD MAJIKAN,BIL.PESERTA,JUMLAH CA,JUMLAH IA,EMAIL,CERAMAH PENCEGAHAN KEMALANGAN JALAN RAYA,CERAMAH KEMALANGAN INDUSTRI/PENYAKIT PEKERJAAN,CERAMAH PROMOSI KESEHATAN/HSP,PEMBERIAN TOPI KELEDAR,CERAMAH OSH DARI MIROS,CERAMAH DARI JKJR,CERAMAH DARI LAIN-LAIN AGENSI,OTHER,OTHER TEXT,CADANGAN PENAMBAHBAIKKAN,PHOTO");
    // $label = explode(",","Email,Pejabat,Majikan,Kod Majikan,Tarikh,Bil Peserta,CA,IA,checkbox1,checkbox2,checkbox3,checkbox4,checkbox5,checkbox6,checkbox7,checkbox8,checkbox8_text,Cadangan,photo1,photo2,photo3,photo4,photo5");
    // $txt_header = 'oshe_email,oshe_pejabat,oshe_majikan,oshe_kod_majikan,oshe_tarikh,oshe_bil_peserta,oshe_ca,oshe_ia,oshe_checkbox1,oshe_checkbox2,oshe_checkbox3,oshe_checkbox4,oshe_checkbox5,oshe_checkbox6,oshe_checkbox7,oshe_checkbox8,oshe_checkbox8_text,oshe_cadangan,oshe_photo1,oshe_photo2,oshe_photo3,oshe_photo4,oshe_photo5';
    $header = explode(',',$txt_header);
     $sql = 'SELECT '.$txt_query.' FROM oshe WHERE '.$created_by.' AND oshe_status = 1 AND oshe_email <> \'\' '; 
     $result = $db->rawQuery($sql);

     if(!$result)
    {
    echo '<script>alert("No Data Found.!!");window.location="oshe";</script>';
    }

?>

<html>
<head>
	<title>Exported Oshe</title>
</head>
<body>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
	</style>
 
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=submitted_oshe.xls");
	?>
 
	<center>
		<h1>All Submitted OSHE Oshe</h1>
	</center>
 
	<table border="1">
		<tr>
			<th>No</th>
            <?php
        foreach ($label as $value)
        {
            echo "<th>".$value."</th>";
        }
        ?>
		
		</tr>
        <?php
        $i=0;
        for($i;$i<count($result);$i++)
        {
            echo "<tr><td>".($i+1)."</td>";
            //===============
            $j=0;
            for($j;$j<count($header);$j++)
            {
                $pattern = "/photo/i";
                $pattern = "/photo/i";

                if(preg_match( $pattern , $header[$j]) &&  $result[$i][ $header[$j] ] != '' &&  $result[$i][ $header[$j] ] != null)
                {
                    echo "<td>https://".$_SERVER['SERVER_NAME']."/uploads/oshe/".$result[$i][ $header[$j] ]."</td>";
                }
                else
                {
                    echo "<td>".$result[$i][ $header[$j] ]."</td>";
                }
            }
            
            //===============-
            echo "</tr>";
        ?>
		
		<?php
        }
        ?>
	</table>
</body>
</html>

<?php
}
?>
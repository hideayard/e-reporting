
<?php

session_start();

if (isset($_SESSION['HTTP_USER_AGENT']))
{
    if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT']))
    {
        /* Prompt for password */
        // exit;
    }
}
else
{
    $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
}

// header("X-Frame-Options: SAMEORIGIN");

// header("Content-Security-Policy: report-uri /__cspreport__;default-src 'none';frame-ancestors 'none';");
// Start a session (which should use cookies over HTTP only).
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("config/functions.php");

//set tgl
date_default_timezone_set("Asia/Jakarta");
$tgl=date('Y-m-d');
$page=isset($_GET['page']) ? $_GET['page'] : "home"; 
$mode=isset($_GET['mode']) ? $_GET['mode'] : ""; 
$d=isset($_GET['d']) ? $_GET['d'] : ""; 
if(!isset($_SESSION['u']))
{
  header('Location:login.php');
  exit(); //hentikan eksekusi kode di login_proses.php
}
$skrg = date("Y-m-d h:i:sa");
//$_SESSION['i']
$id_user=isset($_SESSION['i']) ? $_SESSION['i'] : ""; 
$email=isset($_SESSION['e']) ? $_SESSION['e'] : ""; 
$tipe_user=isset($_SESSION['t']) ? $_SESSION['t'] : ""; 
// echo $id_user;die;
$table = isset($_GET['t']) ? $_GET['t'] : 'inbox';
$selected=array();
$home='';
switch ($page) {
        case 'users' : {
                        $selected[8]=' class="active" ';$judul="Users";
                      }break;
        case 'home' : {
                        $selected[4]=' class="active" ';$judul="Home";$home='active';
                      }break;
        
        default : {
                    
                       $selected[4]=' class="active" ';$judul="Home";  
                      }break;

      }

      $sql = "SELECT * FROM users WHERE user_id = '".$_SESSION['i']."' "; 
      $result = $db->rawQuery($sql);//@mysql_query($sql);
      

// See: http://blog.ircmaxell.com/2013/02/preventing-csrf-attacks.html


// Create a new CSRF token.
// if (! isset($_SESSION['csrf_token'])) {
//     $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
// }

// Check a POST is valid.
if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
    // POST data is valid.
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OSH PRO | Dashboard</title>
  <link rel="icon" href="assets/img/logo.png" type="image/png" sizes="50x50">  
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">

  <link rel="stylesheet" href="dist/css/adminlte.min.css">

  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- /home/al/Downloads/select2-bootstrap.css -->
  <!-- Daterange picker -->
  <link rel="stylesheet" href="dist/css/jquery.datetimepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- toggle -->
  <link href="plugins/bootstrap-toggle-master/css/bootstrap-toggle.css" rel="stylesheet">

<script src="dist/js/jquery-3.5.1.slim.js"></script>

<?php if(($page=="role") || ($page=="settings") || ($page=="users") || ($page=="program") || (strpos($page, 'table') !== false)  || (strpos($page, 'report') !== false)) {   ?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap.min.css">

<?php } ?>

<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>

<?php
if($page=="statistic"){
?>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<script src="dist/js/demo.js"></script>
<?php
}
?>
<style>
  .photo_preview{
      width : 200px;
      height : 200px;
  }
  .dropzone-previews {
          width: 100%;
          border: dashed 1px blue;
          background-color: lightblue;
      }
  .center{text-align:center;}
</style>


<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/dropzone/min/dropzone.min.js"></script> 
<link href="plugins/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css">
<link href="plugins/dropzone/min/basic.min.css" rel="stylesheet" type="text/css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="home" class="nav-link">Home</a>
      </li>
      
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />

      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i><?php //echo $page."=".strpos($page, 'vz')."==".(strpos($page, 'vz') !== false); ?>
          </button>
        </div>
      </div>
    </form>

  </nav>
  <!-- /.navbar -->
<script>
Dropzone.autoDiscover = false;
</script>
<?php require_once ("sidebar.php"); ?>

  <?php
  if(!check_role($page,null) && ($page!="home") && ($page!="logout") && ($page!="login") && ( (strpos($page, 'vz') !== false) < 0 ) && ($page!="profile") )
  {
    echo "<script>Swal.fire(
                      'Info!',
                      'You are not authorized!',
                      'info'
                      );
                console.log('You Are Not Authorized');
                setTimeout(function(){ window.stop();window.location='home'; }, 1000);
                
                </script>";
  }
  else
  {
            if((($tipe_user=="MAINTENANCE") && (strpos($page, 'ucux') !== false) ) || ($page=="logout")  || ($page=="profile")   )
            {
              if (file_exists("".$page.".php")) 
              {
                  include("".$page.".php");
              }
              else 
              {
                  include("error.php");
              }

            }
            else if( ( $tipe_user=="MAINTENANCE") &&  ($page=="home")   )
            {
                  include("ucux.php");
            }
            else if($tipe_user!="MAINTENANCE")
            {
            
            if (file_exists("".$page.".php")) 
            {
                include("".$page.".php");
            }
            else 
            {
                include("error.php");
            }
          }

        }
          ?>

  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">M4D Studio</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<script src="dist/js/jquery.datetimepicker.js"></script>
<!-- jquery.datetimepicker.css -->
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<?php if(($page=="role") || ($page=="settings") || ($page=="users") || ($page=="program") || (strpos($page, 'table') !== false)  || (strpos($page, 'report') !== false)) {   ?>

<!-- DataTables -->
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.6/js/responsive.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/pdfmake.min.js"></script>
<script src="dist/js/buttons.html5.min.js"></script>

<script src="dist/js/jszip.min.js"></script>
<script src="dist/js/buttons.colVis.min.js"></script>
<script src="dist/js/buttons.bootstrap4.min.js"></script>
<script src="dist/js/buttons.print.min.js"></script>
<?php } ?>

<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<?php if(($page=="statistic") || ($page=="statistic")) {   ?>

<!-- FLOT CHARTS -->
<script src="plugins/flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="plugins/flot-old/jquery.flot.resize.min.js"></script>
<?php } ?>

<!-- toggle -->
<script src="plugins/bootstrap-toggle-master/js/bootstrap-toggle.js"></script>
<style type="text/css">/* Chart.js */
@keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style>
<script>
  
  $(document).ready(function() {
	
	setTimeout(function(){
		$('body').addClass('loaded');
		// $('h1').css('color','#222222');
	}, 1000);
	
});

$(function () {

$("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });

  
<?php $d = 1; ?>

 // Filter from database
$('.select2').select2();
     //Initialize Select2 Elements

$('.select2bs4').select2({
  theme: 'bootstrap4'
});

var options2 = new Array();
  options2.push({
			id: 0,
			text: 'LOW'
    });

$('.select2risk').select2({
    theme: 'bootstrap4'
		// data: options2,
		,escapeMarkup: function(markup) {
      console.log(markup);
      if(markup=="LOW")
      {
        return '<i class="fa fa-circle text-success"></i> LOW';
      }
      else if(markup=="MEDIUM")
      {
        return '<i class="fa fa-circle text-warning"></i> MEDIUM';
      }
      else if(markup=="HIGH")
      {
        return '<i class="fa fa-circle text-danger"></i> HIGH';
      }
			
		}
  });

	$('.FilterDB').select2({
    minimumInputLength: 2,
        placeholder: 'Select Program',
        // theme: "material",
        theme: 'bootstrap4',
        // allowClear: true,
        ajax: {
          url: 'select2program.php?filter=yes&d=<?=$d?>',//'filterDB.php',
          dataType: 'json',
          // delay: 50,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      }); 

      $('.FilterDBsesi').select2({
    minimumInputLength: 2,
        placeholder: 'Select an item',
        // theme: "material",
        theme: 'bootstrap4',
        // allowClear: true,
        ajax: {
          url: 'select2sesi.php?filter=yes',//'filterDB.php',
          dataType: 'json',
          // delay: 50,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
      }); 

 //Date picker
    $('#tarikh').datetimepicker({
      timepicker:false,
	format:'d-m-Y',
	formatDate:'Y/m/d',
	// minDate:'-1970/01/02', // yesterday is minimum date
  scrollMonth : false,
    scrollInput : false
    });

  $('#duration').datetimepicker({
      datepicker:false,
	    format:'H:i',
	    step:5
    });
    //Timepicker
    $('#start').datetimepicker({
      datepicker:false,
	    format:'H:i',
	    step:5
    });
    //Timepicker
    $('#stop').datetimepicker({
      datepicker:false,
	    format:'H:i',
	    step:5
    });
    <?php if(($page=="role") || ($page=="settings") || ($page=="users") || ($page=="program") || (strpos($page, 'table') !== false)  || (strpos($page, 'report') !== false)) {   ?>
    var tabel = $('#example2').DataTable({
      // "orderCellsTop": true,
      "paging": true,
      // "lengthChange": false,
      // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
      "paging": true,
      "scrollX": true,
      <?php if(  $page != "settings") { ?>
      "processing": true,
      "serverSide": true,
      <?php } ?>
      "rowReorder": {
            "selector": 'td:nth-child(3)'
        },

        "dom": 'Bfrtip',
        "buttons": [
           'copy', 'csv', 'excel', 'pdf', 'print',
           <?php
           if($page == "oshesubmittedtable")
           {
           ?>
                  {
                        text: 'Export All Data',
                        action: function ( e, dt, node, config ) {
                            window.open("/export.php");
                        }
                    }

            <?php
           }
           ?>
        ],

        <?php
        switch($page)
        {
          case "role" :  {echo '"ajax": "get_data_role.php?id='.$id_user.'&mode=list"';echo ',"order": [[ 1, "desc" ]],'; }break;
          case "users" :  {echo '"ajax": "get_data_users.php?id='.$id_user.'&mode=list"';echo ',"order": [[ 1, "desc" ]],'; }break;
          case "oshereport" : {echo '"ajax": "get_data_oshe_report.php"';}break;
          case "ngoreport" : {echo '"ajax": "get_data_ngo_report.php"';}break;
          case "ucuxreport" : {echo '"ajax": "get_data_table_ucux.php"';}break;
          case "vzreport" : {echo '"ajax": "get_data_vz_report.php"';}break;
          case "program" : {echo '"ajax": "get_data_program.php"';}break;
          case "oshesubmittedtable" : {echo '"ajax": "get_data_oshe.php?id='.$id_user.'&mode=submitted"';echo ',"order": [[ 2, "desc" ]],'; }break;
          case "ngosubmittedtable" : {echo '"ajax": "get_data_ngo.php?id='.$id_user.'&mode=submitted"';echo ',"order": [[ 1, "desc" ]],'; }break;
          case "oshedrafttable" :  {echo '"ajax": "get_data_oshe.php?id='.$id_user.'&mode=draft"';echo ',"order": [[ 2, "desc" ]],'; }break;
          case "ngodrafttable" :  {echo '"ajax": "get_data_ngo.php?id='.$id_user.'&mode=draft"';echo ',"order": [[ 1, "desc" ]],'; }break;
          case "vzsubmittedtable" : {echo '"ajax": "get_data_vz.php?id='.$id_user.'&mode=submitted"';echo ',"order": [[ 1, "desc" ]],'; }break;
          case "vzdrafttable" :  {echo '"ajax": "get_data_vz.php?id='.$id_user.'&mode=draft"';echo ',"order": [[ 1, "desc" ]],'; }break;
        }
        ?>
        
    }); //end of datatables
     <?php } ?>

    
  });

function actiondelete(module,type,module_id)
{
 
  Swal.fire({
  title: 'Are you sure?',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    var data = new FormData();
    data.append("mode", "delete");//module);
    data.append("type", type);
    data.append(module+"_id", module_id);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "action"+module+".php",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
          var rv;
          try {
            rv = JSON.parse(data);
            if(isEmpty(rv))
            {
                    Swal.fire(
                    'Info!',
                    'No Data!',
                    'info'
                    );
                console.log("NO DATA : ", data);
                $("#btnLoadMore").html('Load More');
            }
            else
            {
              if(rv.status==true)
              {
                Swal.fire(
                    'Success!',
                    'Success Delete Data!',
                    'success'
                    );
                console.log("SUCCESS Delete: ", data);
               setTimeout(function(){ location.reload();
                 }, 1000);
              }
              else if(rv.info==1 || rv.info==2)
              {
                Swal.fire(
                      'Success!',
                      'Success Delete Data!',
                      'success'
                      );
                console.log("SUCCESS DELETE : ", data);
                setTimeout(function(){ location.reload();
                 }, 1000);              }

            }
          } catch (e) {
            //error data not json
            Swal.fire(
                    'error!',
                    'Error Delete Data, '+data,
                    'error'
                    );
                
                console.log("ERROR : ", data);
          } 
        },
        error: function (e) {
            console.log("ERROR : ", e);
        }
    });
    
  }
})
 
}
    
function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
      return true;
  }

  
</script>
<script src="dist/js/vfs_fonts.js"></script>

</body>
</html>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();

$csrf_token = isset($_POST['csrf_token'])? $_POST['csrf_token'] : false;
$stoken = isset($_SESSION['csrf_token'])? $_SESSION['csrf_token'] : 'kosong';

// Check a POST is valid.
if ($csrf_token === $stoken) {

    require_once ('config/MysqliDb.php');
    include_once ("config/db.php");
    $db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
    include("config/functions.php");       

    $program_name_ms = isset($_POST['program_name_ms']) ? $_POST['program_name_ms'] : ""; 
    $program_name_en = isset($_POST['program_name_en']) ? $_POST['program_name_en'] : ""; 
    $program_code = isset($_POST['program_code']) ? $_POST['program_code'] : ""; 

    $program_remark = isset($_POST['program_remark']) ? $_POST['program_remark'] : ""; 
    $program_status = isset($_POST['program_status']) ? $_POST['program_status'] : "";  

    $program_pt_id = isset($_POST['program_pt_id']) ? $_POST['program_pt_id'] : "";          

    $id = isset($_SESSION['i']) ? $_SESSION['i'] : "";
    $info = "Insert Sukses!!";
    $tgl = (new \DateTime())->format('Y-m-d H:i:s');
    


    $data = Array ("program_id" => null,
                  "program_name_ms" => $program_name_ms,
                  "program_name_en" => $program_name_en,
                  "program_code" => $program_code,//$sesi_tgl->format('Y-m-d'),
                  "program_remark" => $program_remark,
                  "program_status" => 1,
                  "program_created_by" => $id,
                  "program_created_at" => $tgl,
                  "program_modified_by" => $id,
                  "program_modified_at" => $tgl,
                  "program_pt_id" => $program_pt_id,
                  );

  $hasil = $db->insert ('program', $data);
  

  if($hasil)
  {
      echo 1;
  }
  else
  {
    echo $db->getLastError();
  }

}//end else
else
{
  echo 0;
}


?>